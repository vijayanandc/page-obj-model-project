package projectday;



import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.DataLibrary;

public class Day {
	
	@Test(dataProvider="data")
	public static void meth(String un, String pw , String vendor) throws Throwable {
		
	
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	
	ChromeDriver driver = new ChromeDriver();
	
	driver.manage().window().maximize();
	
	driver.get("https://acme-test.uipath.com/account/login");
	
	driver.findElement(By.id("email")).sendKeys(un);
	driver.findElement(By.id("password")).sendKeys(pw);
	driver.findElementById("buttonLogin").click();
	
	Thread.sleep(2000);
	
	driver.findElementByXPath("(//div[@class='dropdown']/button)[4]").click();
	
	WebElement elevendor = driver.findElementByXPath("//a[text()='Search for Vendor']");
	
	Actions action = new Actions(driver);
	
	action.moveToElement(elevendor).perform();
	
	elevendor.click();
	
	//driver.findElementByXPath("//a[text()='Search for Vendor']").click();
	
	driver.findElementById("vendorTaxID").sendKeys(vendor);
	
	driver.findElementById("buttonSearch").click();
	
	String text = driver.findElementByXPath("//table[@class='table']/tbody/tr[2]/td[1]").getText();
	
	System.out.println(text);
	
	driver.close();
	
	
	
}
	
		@DataProvider(name="data")
		public Object[][] getdata() throws Throwable {
		
		return DataLibrary.readExcelData("Projectday");
				
		
		
	}
	
}
