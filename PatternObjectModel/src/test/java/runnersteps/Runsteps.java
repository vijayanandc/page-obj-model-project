package runnersteps;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\java\\features\\CreateLead.feature", glue= {"com.yalla.pages","stepdefinitions"},
                 monochrome=true, dryRun=false,snippets=SnippetType.CAMELCASE)

//@CucumberOptions(features="src\\test\\java\\features\\CreateLead.feature", glue= {"com.yalla.pages","stepdefinitions"},
//monochrome=true, dryRun=false,snippets=SnippetType.CAMELCASE,tags="@smoke")

public class Runsteps {

}
