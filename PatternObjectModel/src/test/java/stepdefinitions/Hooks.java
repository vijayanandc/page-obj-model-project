package stepdefinitions;

import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase {
	
	//Hooks are like Annotations & Reports class in POM
	// we need to implement all before methods in this method order = suite, test, class , method
	@Before
	public void beforescenario(Scenario sc)
	{
		System.out.println(sc.getName());
		System.out.println(sc.getId());
		startReport();
		test = extent.createTest(sc.getName(), sc.getId());
	    test.assignAuthor("vijay");
	    test.assignCategory("smoke"); 
	    startApp("chrome", "http://leaftaps.com/opentaps");
	}
	
	//we need to implement all after methods in this method order = method,class,test,suite
	@After
	public void afterscenario(Scenario sc)
	{
		
		System.out.println(sc.getStatus());
		close();
		stopReport();
	}

}
