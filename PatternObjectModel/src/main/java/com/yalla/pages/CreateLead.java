package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLead extends Annotations{ 

	public CreateLead() {
       PageFactory.initElements(driver, this);
	} 

	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement elecompname;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement elefname;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement elelname;
	@FindBy(how=How.NAME, using="submitButton") WebElement elesubmit;
	
	@And("Enter comp name as (.*)")
	public CreateLead enterCompName(String data) {
		clearAndType(elecompname, data);  
		return this;

	}
	
	@And("Enter firstname as (.*)")
	public CreateLead enterfirstName(String data) {
		clearAndType(elefname, data);  
		return this;

	}
	
	@And("Enter Lastname as (.*)")
	public CreateLead enterlastname(String data) {
		clearAndType(elelname, data);  
		return this;

	}
	
	@When("click on Createlead button")
	public ViewLead clicksubmit() { 
		click(elesubmit);
		return new ViewLead();

	}

}







