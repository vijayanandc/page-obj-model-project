package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "vijay";
		category = "smoke";
		excelFileName = "TC002";
		//This is for getting data from single WB with different sheets
		//sheetname = "TC002"
	} 

	@Test(dataProvider="fetchData") 
	public void createlaead(String uName, String pwd, String Coname , String Fname , String Lname) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickcrmsfa()
		.clickLeads()
		.clickCreateLeads()
		.enterCompName(Coname)
		.enterfirstName(Fname)
		.enterlastname(Lname)
		.clicksubmit()
		.verifyfn();
		
		
		
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}






